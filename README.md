# Post Tag Test

What is Post Tag Test ? Post Tag Test is a project made to learn about golang deeply into Restful API.

## Installation Guide
```
git clone https://gitlab.com/urcane/post-tag-test.git
set up database.
set up .env, copy .env.example and configure just like what you need.
go run migrate.go // this is for migrate the database
go run main.go // run for development, address would be https://localhost:3000/
```
## Documentation / Usage Knowledge

you can read the documentation about this api on this link: (Postman Documentation):(https://documenter.getpostman.com/view/15645557/2sA3QpDZj5), dont forgot to set up Postman Environment if you tried this project on local

## Demo
Demo will be there very soon. please be patient :D.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
