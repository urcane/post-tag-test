package middleware

import (
	"context"
	"errors"
	"github.com/urcane/post-test-golang/helper"
	"github.com/urcane/post-test-golang/model/web"
	"net/http"
	"strings"
)

type AuthMiddleware struct {
	Handler http.Handler
}

func NewAuthMiddleware(handler http.Handler) *AuthMiddleware {
	return &AuthMiddleware{Handler: handler}
}

func (middleware *AuthMiddleware) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	if request.URL.Path == "/api/register" || request.URL.Path == "/api/login" {
		middleware.Handler.ServeHTTP(writer, request)
		return
	}

	authHeader := request.Header.Get("Authorization")
	if authHeader == "" {
		unauthorized(writer)
		return
	}

	parts := strings.Split(authHeader, " ")
	if len(parts) != 2 || parts[0] != "Bearer" {
		unauthorized(writer)
		return
	}

	tokenString := parts[1]
	claims, err := helper.ValidateJWT(tokenString)
	if err != nil {
		unauthorized(writer)
		return
	}

	ctx := context.WithValue(request.Context(), "claims", claims)
	request = request.WithContext(ctx)

	middleware.Handler.ServeHTTP(writer, request)
}

func unauthorized(writer http.ResponseWriter) {
	writer.Header().Set("Content-Type", "application/json")
	writer.WriteHeader(http.StatusUnauthorized)

	webResponse := web.WebResponse{
		Code:   http.StatusUnauthorized,
		Status: "UNAUTHORIZED",
	}

	helper.WriteToResponseBody(writer, webResponse)
}

func GetClaimsFromContext(ctx context.Context) (*helper.JWTClaim, error) {
	claims, ok := ctx.Value("claims").(*helper.JWTClaim)
	if !ok {
		return nil, errors.New("claims not found in context")
	}
	return claims, nil
}
