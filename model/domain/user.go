package domain

type User struct {
	ID       int
	Username string
	Password string
	Email    string
	Role     string
	Token    string
}
