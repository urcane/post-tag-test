package tag_repository

import (
	"context"
	"database/sql"
	"errors"
	"github.com/urcane/post-test-golang/helper"
	"github.com/urcane/post-test-golang/model/domain"
)

type TagRepositoryImpl struct{}

func NewTagRepository() TagRepository {
	return &TagRepositoryImpl{}
}

func (TagRepositoryImpl) Save(ctx context.Context, tx *sql.Tx, tag domain.Tag) domain.Tag {
	query := "INSERT INTO tags (label) VALUES ($1) RETURNING id"
	err := tx.QueryRowContext(ctx, query, tag.Label).Scan(&tag.ID)
	helper.PanicIfError(err)
	return tag
}

func (repo *TagRepositoryImpl) Update(ctx context.Context, tx *sql.Tx, tag domain.Tag) domain.Tag {
	query := "UPDATE tags SET label = $1 WHERE id = $2"
	_, err := tx.ExecContext(ctx, query, tag.Label, tag.ID)
	helper.PanicIfError(err)

	repo.UnlinkPost(ctx, tx, tag)

	for _, post := range tag.Posts {

		_, err := tx.ExecContext(ctx, "INSERT INTO post_tags (post_id, tag_id) VALUES ($1, $2)", post.ID, tag.ID)
		helper.PanicIfError(err)
	}

	return tag
}

func (TagRepositoryImpl) Delete(ctx context.Context, tx *sql.Tx, tag domain.Tag) {
	query := "DELETE FROM tags WHERE id = $1"
	_, err := tx.ExecContext(ctx, query, tag.ID)
	helper.PanicIfError(err)
}

func (TagRepositoryImpl) FindById(ctx context.Context, tx *sql.Tx, tagId int) (domain.Tag, error) {
	query := "SELECT id, label FROM tags WHERE id = $1"
	row := tx.QueryRowContext(ctx, query, tagId)
	tag := domain.Tag{}
	err := row.Scan(&tag.ID, &tag.Label)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return tag, errors.New("tag not found")
		}
		helper.PanicIfError(err)
	}
	return tag, nil
}

func (TagRepositoryImpl) FindByLabel(ctx context.Context, tx *sql.Tx, tagLabel string) (domain.Tag, error) {
	query := "SELECT id, label FROM tags WHERE label = $1"
	row := tx.QueryRowContext(ctx, query, tagLabel)
	tag := domain.Tag{}
	err := row.Scan(&tag.ID, &tag.Label)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return tag, errors.New("tag not found")
		}
		helper.PanicIfError(err)
	}
	return tag, nil
}

func (TagRepositoryImpl) FindAll(ctx context.Context, tx *sql.Tx) []domain.Tag {
	query := "SELECT id, label FROM tags"
	rows, err := tx.QueryContext(ctx, query)
	helper.PanicIfError(err)
	defer rows.Close()

	var tags []domain.Tag
	for rows.Next() {
		tag := domain.Tag{}
		err := rows.Scan(&tag.ID, &tag.Label)
		helper.PanicIfError(err)
		tags = append(tags, tag)
	}
	return tags
}

func (TagRepositoryImpl) SaveWithPosts(ctx context.Context, tx *sql.Tx, tag domain.Tag) domain.Tag {
	tag = TagRepositoryImpl{}.Save(ctx, tx, tag)

	for _, post := range tag.Posts {
		_, err := tx.ExecContext(ctx, "INSERT INTO post_tags (post_id, tag_id) VALUES ($1, $2)", post.ID, tag.ID)
		helper.PanicIfError(err)
	}

	return tag
}

func (TagRepositoryImpl) FindByIDWithPosts(ctx context.Context, tx *sql.Tx, tagID int) (domain.Tag, error) {
	tag, err := TagRepositoryImpl{}.FindById(ctx, tx, tagID)
	if err != nil {
		return domain.Tag{}, err
	}

	query := "SELECT p.id, p.title, p.content, ps.status, p.publish_date FROM posts p JOIN post_tags pt ON p.id = pt.post_id JOIN post_statuses ps ON p.status_id = ps.id WHERE pt.tag_id = $1"
	rows, err := tx.QueryContext(ctx, query, tagID)
	helper.PanicIfError(err)
	defer rows.Close()

	for rows.Next() {
		var post domain.Post
		var status domain.Status
		err := rows.Scan(&post.ID, &post.Title, &post.Content, &status.Status, &post.PublishDate)
		helper.PanicIfError(err)
		post.Status = &status
		tag.Posts = append(tag.Posts, &post)
	}

	return tag, nil
}

func (TagRepositoryImpl) UnlinkPost(ctx context.Context, tx *sql.Tx, tag domain.Tag) {
	query := "DELETE FROM post_tags WHERE tag_id = $1"
	_, err := tx.ExecContext(ctx, query, tag.ID)
	helper.PanicIfError(err)
}
