package post_repository

import (
	"context"
	"database/sql"
	"errors"
	"github.com/urcane/post-test-golang/helper"
	"github.com/urcane/post-test-golang/model/domain"
)

type PostRepositoryImpl struct{}

func NewPostRepository() PostRepository {
	return &PostRepositoryImpl{}
}

func (PostRepositoryImpl) Save(ctx context.Context, tx *sql.Tx, post domain.Post) domain.Post {
	query := "INSERT INTO posts (title, content, status_id, publish_date) VALUES ($1, $2, $3, $4) RETURNING id"
	err := tx.QueryRowContext(ctx, query, post.Title, post.Content, post.Status.ID, post.PublishDate).Scan(&post.ID)
	helper.PanicIfError(err)
	return post
}

func (repo *PostRepositoryImpl) Update(ctx context.Context, tx *sql.Tx, post domain.Post, user domain.User) domain.Post {
	query := "UPDATE posts SET title = $1, content = $2, status_id = $3, publish_date = $4 WHERE id = $5"
	_, err := tx.ExecContext(ctx, query, post.Title, post.Content, post.Status.ID, post.PublishDate, post.ID)
	helper.PanicIfError(err)

	repo.UnlinkTag(ctx, tx, post)

	for _, tag := range post.Tags {
		_, err := tx.ExecContext(ctx, "INSERT INTO post_tags (post_id, tag_id) VALUES ($1, $2)", post.ID, tag.ID)
		helper.PanicIfError(err)
	}

	return post
}

func (PostRepositoryImpl) Delete(ctx context.Context, tx *sql.Tx, post domain.Post) {
	query := "DELETE FROM posts WHERE id = $1"
	_, err := tx.ExecContext(ctx, query, post.ID)
	helper.PanicIfError(err)
}

func (PostRepositoryImpl) FindById(ctx context.Context, tx *sql.Tx, postId int) (domain.Post, error) {
	query := "SELECT p.id, p.title, p.content, p.publish_date, ps.id, ps.status FROM posts p JOIN post_statuses ps ON p.status_id = ps.id WHERE p.id = $1"
	row := tx.QueryRowContext(ctx, query, postId)
	post := domain.Post{}
	status := domain.Status{}

	err := row.Scan(&post.ID, &post.Title, &post.Content, &post.PublishDate, &status.ID, &status.Status)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return post, errors.New("post not found")
		}
		helper.PanicIfError(err)
	}

	post.Status = &status

	return post, nil
}

func (PostRepositoryImpl) FindAll(ctx context.Context, tx *sql.Tx) []domain.Post {

	query := "SELECT p.id, p.title, p.content, p.publish_date, ps.id status_id, ps.status FROM posts p JOIN post_statuses ps ON p.status_id = ps.id"
	rows, err := tx.QueryContext(ctx, query)
	helper.PanicIfError(err)
	defer rows.Close()

	var posts []domain.Post
	for rows.Next() {
		post := domain.Post{}
		status := domain.Status{}
		err := rows.Scan(&post.ID, &post.Title, &post.Content, &post.PublishDate, &status.ID, &status.Status)
		helper.PanicIfError(err)

		post.Status = &status

		posts = append(posts, post)
	}
	return posts
}

func (PostRepositoryImpl) SaveWithTags(ctx context.Context, tx *sql.Tx, post domain.Post) domain.Post {
	post = PostRepositoryImpl{}.Save(ctx, tx, post)

	for _, tag := range post.Tags {
		_, err := tx.ExecContext(ctx, "INSERT INTO post_tags (post_id, tag_id) VALUES ($1, $2)", post.ID, tag.ID)
		helper.PanicIfError(err)
	}

	return post
}

func (PostRepositoryImpl) FindByIDWithTags(ctx context.Context, tx *sql.Tx, postID int) (domain.Post, error) {
	post, err := PostRepositoryImpl{}.FindById(ctx, tx, postID)
	if err != nil {
		return domain.Post{}, err
	}

	rows, err := tx.QueryContext(ctx, "SELECT t.id, t.label FROM tags t JOIN post_tags pt ON t.id = pt.tag_id WHERE pt.post_id = $1", postID)
	helper.PanicIfError(err)
	defer rows.Close()

	for rows.Next() {
		tag := domain.Tag{}
		err := rows.Scan(&tag.ID, &tag.Label)
		helper.PanicIfError(err)
		post.Tags = append(post.Tags, &tag)
	}

	return post, nil
}

func (PostRepositoryImpl) FindByTagLabel(ctx context.Context, tx *sql.Tx, tagLabel string) ([]domain.Post, error) {
	query := `
        SELECT p.id, p.title, p.content, p.publish_date,
               t.id, t.label, 
               ps.id, ps.status
        FROM posts p
        JOIN post_tags pt ON p.id = pt.post_id
        JOIN tags t ON pt.tag_id = t.id
        LEFT JOIN post_statuses ps ON p.status_id = ps.id
        WHERE t.label = $1`

	rows, err := tx.QueryContext(ctx, query, tagLabel)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	posts := make(map[int]*domain.Post)
	for rows.Next() {
		var post domain.Post
		var tag domain.Tag
		var status domain.Status
		err := rows.Scan(&post.ID, &post.Title, &post.Content, &post.PublishDate, &tag.ID, &tag.Label, &status.ID, &status.Status)
		helper.PanicIfError(err)

		post.Status = &status

		if existingPost, ok := posts[post.ID]; ok {
			existingPost.Tags = append(existingPost.Tags, &tag)
		} else {
			post.Tags = []*domain.Tag{&tag}
			posts[post.ID] = &post
		}
	}

	var result []domain.Post
	for _, post := range posts {
		result = append(result, *post)
	}

	return result, nil
}

func (PostRepositoryImpl) UnlinkTag(ctx context.Context, tx *sql.Tx, post domain.Post) {
	query := "DELETE FROM post_tags WHERE post_id = $1"
	_, err := tx.ExecContext(ctx, query, post.ID)
	helper.PanicIfError(err)
}
