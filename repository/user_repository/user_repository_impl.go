package user_repository

import (
	"context"
	"database/sql"
	"errors"
	"github.com/urcane/post-test-golang/helper"
	"github.com/urcane/post-test-golang/model/domain"
)

type UserRepositoryImpl struct{}

func NewUserRepository() UserRepository {
	return &UserRepositoryImpl{}
}

func (UserRepositoryImpl) Save(ctx context.Context, tx *sql.Tx, user domain.User) domain.User {
	query := "INSERT INTO users (username, password, email, role, token) VALUES ($1, $2, $3, $4, $5) RETURNING id"
	err := tx.QueryRowContext(ctx, query, user.Username, user.Password, user.Email, user.Role, user.Token).Scan(&user.ID)

	helper.PanicIfError(err)
	return user
}

func (UserRepositoryImpl) FindByUsername(ctx context.Context, tx *sql.Tx, username string) (domain.User, error) {
	query := "SELECT id, username, password, email, role, token FROM users WHERE username = $1"
	row := tx.QueryRowContext(ctx, query, username)
	user := domain.User{}
	err := row.Scan(&user.ID, &user.Username, &user.Password, &user.Email, &user.Role, &user.Token)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return domain.User{}, errors.New("user not found")
		}
		return domain.User{}, err
	}
	return user, nil
}

func (UserRepositoryImpl) UpdateToken(ctx context.Context, tx *sql.Tx, userID int, token string) {
	query := "UPDATE users SET token = $1 WHERE id = $2"
	_, err := tx.ExecContext(ctx, query, token, userID)
	helper.PanicIfError(err)
}
